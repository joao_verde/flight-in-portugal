package com.flightinportugal.flightinformation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class FlightInformationApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightInformationApplication.class, args);
	}

}

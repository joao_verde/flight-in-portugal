package com.flightinportugal.flightinformation.configuration;

import com.flightinportugal.flightinformation.model.calls.RequestPayload;
import com.flightinportugal.flightinformation.service.database.ApiCallService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class RestHttpCallsLoggingFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestHttpCallsLoggingFilter.class);
    private final ApiCallService apiCallService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("########## Initiating Custom filter ##########");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String uri = request.getScheme() + "://" +
                request.getServerName() +
                ("http".equals(request.getScheme()) && request.getServerPort() == 80 || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) +
                request.getRequestURI() +
                (request.getQueryString() != null ? "?" + request.getQueryString() : "");

        RequestPayload requestPayload = RequestPayload.builder()
                .uri(uri)
                .method(request.getMethod())
                .build();

        LOGGER.info("Logging Request  {}", requestPayload);

        //call next filter in the filter chain
        filterChain.doFilter(request, response);

        if(request.getRequestURI().equals("/flight/avg")){
            apiCallService.createNewApiCall(requestPayload);
        }

    }

    @Override
    public void destroy() {
        LOGGER.warn("Destructing filter :{}", this);
    }
}

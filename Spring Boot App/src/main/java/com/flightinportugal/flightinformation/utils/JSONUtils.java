package com.flightinportugal.flightinformation.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtils {
    public static JsonNode convertStringToJsonNode(String jsonString) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(jsonString);
    }

    public static String getJsonNodeFieldAsString(String key, JsonNode jsonNode) {
        return jsonNode.get(key) == null ? null : jsonNode.get(key).asText();
    }
}

package com.flightinportugal.flightinformation.model.calls;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("api_calls")
@Builder
@Data
public class ApiCall {
    @Id
    private String id;
    private String status;
    private RequestPayload requestPayload;
    //private String responsePayload;
}

package com.flightinportugal.flightinformation.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FlightBagAvg implements Serializable {
    @JsonProperty("bag1_average")
    private double bagOneAvg;
    @JsonProperty("bag2_average")
    private double bagTwoAvg;
}

package com.flightinportugal.flightinformation.model.backend;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FlightBackendData {
    private String cityFrom;
    private double price;
    @JsonProperty("bags_price")
    private FlightBackendBag flightBackendBag;
}

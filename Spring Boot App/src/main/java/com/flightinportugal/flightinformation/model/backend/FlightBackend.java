package com.flightinportugal.flightinformation.model.backend;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FlightBackend {
    @JsonProperty("data")
    private List<FlightBackendData> flightBackendDataList;
}

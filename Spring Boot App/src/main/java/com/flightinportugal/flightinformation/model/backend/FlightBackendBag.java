package com.flightinportugal.flightinformation.model.backend;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FlightBackendBag {
    @JsonProperty("1")
    private double bagOne;
    @JsonProperty("2")
    private double bagTwo;
}

package com.flightinportugal.flightinformation.model.calls;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestPayload {
    private String uri;
    private String method;
    //private String body;
    //private String header;
}

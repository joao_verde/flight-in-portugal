package com.flightinportugal.flightinformation.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FlightDestAvg implements Serializable {
    private String airportName;
    private String currency;
    @JsonProperty("flights_number")
    private int numberOfFlights;
    @JsonProperty("price_average")
    private double priceAverage;
    @JsonProperty("bags_price")
    private FlightBagAvg flightDtoBagAvg;
}
package com.flightinportugal.flightinformation.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.flightinportugal.flightinformation.exception.BackendExceptionHandler;
import com.flightinportugal.flightinformation.exception.JsonMalformedException;
import com.flightinportugal.flightinformation.utils.JSONUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


@Service
@RequiredArgsConstructor
@Slf4j
public class LocationService {

    @Autowired
    RestTemplate restTemplate;

    public String getAirportName(String iataCode) {
        try {
            log.info("Calling backend location api to retrieve information of the airport name based on iata code {}", iataCode);

            String baseUrlLocation = "https://api.skypicker.com" + "/locations";

            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(baseUrlLocation)
                    .queryParam("term", iataCode)
                    .queryParam("location_types", "airport")
                    .queryParam("limit", 1);

            ResponseEntity<String> locationBackend = restTemplate.getForEntity(uriBuilder.toUriString(), String.class);

            JsonNode jsonNode = JSONUtils.convertStringToJsonNode(locationBackend.getBody());

            String airportName = jsonNode.at("/locations/0/name").asText();

            log.info("Backend location response is: {}", airportName);

            return airportName;
        } catch (HttpStatusCodeException ex) {
            throw new BackendExceptionHandler(ex.getStatusCode(), ex.getMessage());
        }
        catch (JsonProcessingException ex) {
            throw new JsonMalformedException();
        }
    }
}

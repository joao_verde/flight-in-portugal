package com.flightinportugal.flightinformation.service;

import com.flightinportugal.flightinformation.exception.BackendDataException;
import com.flightinportugal.flightinformation.exception.BackendExceptionHandler;
import com.flightinportugal.flightinformation.model.FlightBagAvg;
import com.flightinportugal.flightinformation.model.FlightDestAvg;
import com.flightinportugal.flightinformation.model.backend.FlightBackend;
import com.flightinportugal.flightinformation.model.backend.FlightBackendData;
import com.google.common.base.Joiner;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

@Service
@Slf4j
public class FlightService {

    private LocationService locationService;

    public FlightService(LocationService locationService) {
        this.locationService = locationService;
    }

    @Autowired
    RestTemplate restTemplate;

    public FlightBackend getFlightBackendData(String flyFrom, String flyTo, String currency, String dateFrom, String dateTo, String airlines) {
        try {
            log.info("Calling backend flights api to retrieve information of the itinerary from {} to {}", flyFrom, flyTo);

            String baseUrlFlights = "https://api.skypicker.com" + "/flights";

            UriComponentsBuilder uriBuilderFlyFromToFlyTo = buildUriBuilder(flyFrom, flyTo, currency, dateFrom, dateTo, baseUrlFlights, airlines);

            ResponseEntity<FlightBackend> flightsBackendResp = restTemplate.getForEntity(uriBuilderFlyFromToFlyTo.toUriString(), FlightBackend.class);
            log.info("Backend flights server response from {} to {}: {}", flyFrom, flyTo, flightsBackendResp.getBody());

            if (Objects.requireNonNull(flightsBackendResp.getBody()).getFlightBackendDataList().isEmpty()) {
                throw new BackendDataException();
            }

            return flightsBackendResp.getBody();
        } catch (
                HttpStatusCodeException ex) {
            throw new BackendExceptionHandler(ex.getStatusCode(), ex.getMessage());
        }
    }

    public UriComponentsBuilder buildUriBuilder(String flyFrom, String flyTo, String currency, String dateFrom, String dateTo, String baseUrlFlights, String airlines) {
        UriComponentsBuilder uriBuilderFlyFromToFlyTo = UriComponentsBuilder.fromUriString(baseUrlFlights)
                // Add query parameter
                .queryParam("partner", "picky")
                .queryParam("flyFrom", flyFrom)
                .queryParam("fly_to", flyTo)
                .queryParam("curr", currency);
        if (dateFrom != null) {
            uriBuilderFlyFromToFlyTo.queryParam("dateFrom",dateFrom);
        }
        if (dateTo != null) {
            uriBuilderFlyFromToFlyTo.queryParam("dateTo",dateTo);
        }
        if (airlines != null) {
            uriBuilderFlyFromToFlyTo.queryParam("select_airlines",airlines);
        }
        return uriBuilderFlyFromToFlyTo;
    }

    @Cacheable(cacheNames = "flights")
    public Map<String, FlightDestAvg> getFlightsInformation(String firstDest, String secondDest, String currency, String dateFrom, String dateTo, String airlines) {
        List<String> destinations = Arrays.asList(firstDest + "->" + secondDest, secondDest + "->" + firstDest);
        log.info("Itinerary list: {}", String.join(",", destinations));
        Map<String, FlightDestAvg> flightDtoResponseMap = new HashMap<>();
        for (String dest : destinations) {
            String[] itinerary = dest.split("->");

            FlightBackend flightDest = getFlightBackendData(itinerary[0], itinerary[1], currency, dateFrom, dateTo, airlines);
            double flightAvg = flightDest.getFlightBackendDataList().stream().mapToDouble(FlightBackendData::getPrice).average().orElse(0.0);
            double flightBagOneAvg = flightDest.getFlightBackendDataList().stream().mapToDouble(value -> value.getFlightBackendBag().getBagOne()).average().orElse(0.0);
            double flightBagTwoAvg = flightDest.getFlightBackendDataList().stream().mapToDouble(value -> value.getFlightBackendBag().getBagTwo()).average().orElse(0.0);

            FlightDestAvg flightDestAvg = new FlightDestAvg();
            flightDestAvg.setAirportName(locationService.getAirportName(itinerary[0]));
            flightDestAvg.setCurrency(currency);
            flightDestAvg.setNumberOfFlights(flightDest.getFlightBackendDataList().size());
            flightDestAvg.setPriceAverage(Precision.round(flightAvg, 2));
            flightDestAvg.setFlightDtoBagAvg(new FlightBagAvg(Precision.round(flightBagOneAvg, 2), Precision.round(flightBagTwoAvg, 2)));
            flightDtoResponseMap.put(itinerary[0], flightDestAvg);
        }
        log.info("Flights information between {} -> {} or {} -> {} ", firstDest, secondDest, firstDest, secondDest);
        log.info("API Response: {} ", Joiner.on(",").withKeyValueSeparator("=").join(flightDtoResponseMap));
        return flightDtoResponseMap;
    }

    public String please(String iata){
        return locationService.getAirportName(iata);
    }

}

package com.flightinportugal.flightinformation.service.database;

import com.flightinportugal.flightinformation.model.calls.ApiCall;
import com.flightinportugal.flightinformation.model.calls.DeleteDto;
import com.flightinportugal.flightinformation.model.calls.RequestPayload;
import com.flightinportugal.flightinformation.repository.ApiCallRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApiCallService {
    private final ApiCallRepository apiCallRepository;

    public List<ApiCall> getAllCalls() {
        return apiCallRepository.findAll();
    }

    public DeleteDto deleteAllCalls() {
        DeleteDto deleteDto = new DeleteDto();
        if (!getAllCalls().isEmpty()) {
            apiCallRepository.deleteAll();
            deleteDto.setAction("All api calls has been removed from database");
        } else {
            deleteDto.setAction("There aren't calls to delete");
        }
        return deleteDto;
    }

    public ApiCall createNewApiCall(RequestPayload requestPayload) {
        ApiCall apiCall = ApiCall.builder()
                .status("SUBMITTED")
                .requestPayload(requestPayload)
                .build();
        apiCallRepository.save(apiCall);
        return apiCall;
    }
}

package com.flightinportugal.flightinformation.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UnauthorizedTargetInvocation extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private final String details;
}

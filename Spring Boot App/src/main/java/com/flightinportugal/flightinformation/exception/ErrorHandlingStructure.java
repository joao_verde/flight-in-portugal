package com.flightinportugal.flightinformation.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorHandlingStructure {
    private HttpStatus status;
    private String message;
}

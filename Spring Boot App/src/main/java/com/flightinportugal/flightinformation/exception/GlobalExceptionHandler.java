package com.flightinportugal.flightinformation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BackendExceptionHandler.class)
    protected ResponseEntity<Object> handleUserNotFound(BackendExceptionHandler backendExceptionHandler) {
        ErrorHandlingStructure errorHandlingStructure = new ErrorHandlingStructure(backendExceptionHandler.getHttpStatus(), backendExceptionHandler.getDetails());
        return new ResponseEntity<>(errorHandlingStructure, backendExceptionHandler.getHttpStatus());
    }

    @ExceptionHandler(value = BackendDataException.class)
    protected ResponseEntity<Object> handleNoData() {
        ErrorHandlingStructure errorHandlingStructure = new ErrorHandlingStructure(HttpStatus.NOT_FOUND,"No flights found for the specified period");
        return new ResponseEntity<>(errorHandlingStructure, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = JsonMalformedException.class)
    protected ResponseEntity<Object> handleJsonMalformedException() {
        ErrorHandlingStructure errorHandlingStructure = new ErrorHandlingStructure(HttpStatus.INTERNAL_SERVER_ERROR,"JSON Malformed Exception");
        return new ResponseEntity<>(errorHandlingStructure, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = UnauthorizedTargetInvocation.class)
    public ResponseEntity<Object> targetInvocationException(UnauthorizedTargetInvocation unauthorizedTargetInvocation) {
        ErrorHandlingStructure errorHandlingStructure = new ErrorHandlingStructure(HttpStatus.NOT_FOUND, unauthorizedTargetInvocation.getDetails());
        return new ResponseEntity<>(errorHandlingStructure, HttpStatus.NOT_FOUND);
    }
}

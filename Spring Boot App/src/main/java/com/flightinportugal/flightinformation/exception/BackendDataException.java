package com.flightinportugal.flightinformation.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BackendDataException extends RuntimeException {
    private static final long serialVersionUID = 1L;
}

package com.flightinportugal.flightinformation.controller;

import com.flightinportugal.flightinformation.model.FlightDestAvg;
import com.flightinportugal.flightinformation.service.FlightService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
public class FlightController {

    private final FlightService flightService;

    @GetMapping("/flight/avg")
    public ResponseEntity<Map<String, FlightDestAvg>> getAvgPriceTotalFlights(@RequestParam(name = "dest1") String firstDest,
                                                                              @RequestParam(name = "dest2") String secondDest,
                                                                              @RequestParam(name = "curr") String currency,
                                                                              @RequestParam(required = false, name = "dateFrom") String dateFrom,
                                                                              @RequestParam(required = false, name = "dateTo") String dateTo,
                                                                              @RequestParam(required = false, name = "select_airlines") String airlines) {

        return new ResponseEntity<>(flightService.getFlightsInformation(firstDest, secondDest, currency, dateFrom, dateTo, airlines), HttpStatus.OK);
    }
}

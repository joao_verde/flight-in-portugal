package com.flightinportugal.flightinformation.controller;

import com.flightinportugal.flightinformation.model.calls.ApiCall;
import com.flightinportugal.flightinformation.model.calls.DeleteDto;
import com.flightinportugal.flightinformation.service.database.ApiCallService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ApiCallController {
    private final ApiCallService apiCallService;

    @GetMapping("/api-calls")
    public ResponseEntity<List<ApiCall>> listAllApisCalls() {
        return new ResponseEntity<>(apiCallService.getAllCalls(), HttpStatus.OK);
    }

    @DeleteMapping("/api-calls")
    public ResponseEntity<DeleteDto> deleteAllApisCalls() {
        return new ResponseEntity<>(apiCallService.deleteAllCalls(), HttpStatus.OK);
    }

}
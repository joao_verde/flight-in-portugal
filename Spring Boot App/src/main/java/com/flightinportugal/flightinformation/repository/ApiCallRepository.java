package com.flightinportugal.flightinformation.repository;

import com.flightinportugal.flightinformation.model.calls.ApiCall;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiCallRepository extends MongoRepository<ApiCall, String> {
}

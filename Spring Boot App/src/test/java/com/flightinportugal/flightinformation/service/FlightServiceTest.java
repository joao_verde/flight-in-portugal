package com.flightinportugal.flightinformation.service;

import com.flightinportugal.flightinformation.exception.BackendDataException;
import com.flightinportugal.flightinformation.model.FlightBagAvg;
import com.flightinportugal.flightinformation.model.FlightDestAvg;
import com.flightinportugal.flightinformation.model.backend.FlightBackend;
import com.flightinportugal.flightinformation.model.backend.FlightBackendBag;
import com.flightinportugal.flightinformation.model.backend.FlightBackendData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class FlightServiceTest {

    @Mock
    private LocationService locationService;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private FlightService flightService = new FlightService(locationService);

    @Test
    public void getFlightBackendData_withSuccess_ReturnsListOfFlights() {
        // arrange
        String flyFrom = "OPO";
        String flyTo = "LIS";
        String curr = "EUR";
        String dateFrom = "20/02/2021";
        String dateTo = "20/04/2021";

        UriComponentsBuilder uriBuilderFlyFromToFlyTo = UriComponentsBuilder.fromUriString("https://api.skypicker.com/flights")
                .queryParam("partner", "picky")
                .queryParam("flyFrom", flyFrom)
                .queryParam("fly_to", flyTo)
                .queryParam("curr", curr)
                .queryParam("dateFrom", dateFrom)
                .queryParam("dateTo", dateTo);

        FlightBackendBag flightOneBckBag = new FlightBackendBag(20, 10);
        FlightBackendBag flightTwoBckBag = new FlightBackendBag(30, 20);

        FlightBackendData flightOneBckData = new FlightBackendData("OPO", 20, flightOneBckBag);
        FlightBackendData flightTwoBckData = new FlightBackendData("OPO", 30, flightTwoBckBag);

        List<FlightBackendData> flightBackendDataList = Arrays.asList(flightOneBckData, flightTwoBckData);

        FlightBackend flightBackend = FlightBackend.builder()
                .flightBackendDataList(flightBackendDataList)
                .build();

        // act
        Mockito
                .when(restTemplate.getForEntity(uriBuilderFlyFromToFlyTo.toUriString(), FlightBackend.class))
                .thenReturn(new ResponseEntity(flightBackend, HttpStatus.OK));

        FlightBackend actualFlightBackend = flightService.getFlightBackendData(flyFrom, flyTo, curr, dateFrom, dateTo, null);
        // assert
        Assert.assertEquals(flightBackend, actualFlightBackend);
    }

    @Test
    public void getFlightBackendData_withNoDataFlights_ThrowNotFoundException() {
        // arrange
        String flyFrom = "OPO";
        String flyTo = "LIS";
        String curr = "EUR";
        String dateFrom = "20/02/2021";
        String dateTo = "20/04/2021";

        UriComponentsBuilder uriBuilderFlyFromToFlyToOne = UriComponentsBuilder.fromUriString("https://api.skypicker.com/flights")
                .queryParam("partner", "picky")
                .queryParam("flyFrom", flyFrom)
                .queryParam("fly_to", flyTo)
                .queryParam("curr", curr)
                .queryParam("dateFrom", dateFrom)
                .queryParam("dateTo", dateTo);

        List<FlightBackendData> flightBackendDataListDest1 = new ArrayList<>();
        List<FlightBackendData> flightBackendDataListDest2 = new ArrayList<>();

        FlightBackend flightBackendDest1 = new FlightBackend(flightBackendDataListDest1);
        FlightBackend flightBackendDest2 = new FlightBackend(flightBackendDataListDest2);

        // act
        Mockito
                .when(restTemplate.getForEntity(Mockito.anyString(), Matchers.any(Class.class)))
                .thenReturn(new ResponseEntity(flightBackendDest1, HttpStatus.OK), new ResponseEntity(flightBackendDest2, HttpStatus.OK));

        // assert
        Assertions.assertThrows(BackendDataException.class, () ->flightService.getFlightBackendData(flyFrom, flyTo, curr, dateFrom, dateTo,null));

    }

    @Test
    public void getFlightsInformation_withSuccess_ReturnsAvgPriceFlights() {
        // arrange
        String flyFrom = "OPO";
        String flyTo = "LIS";
        String curr = "EUR";
        String dateFrom = "20/02/2021";
        String dateTo = "20/04/2021";

        FlightBackendBag flightOneBckBag = new FlightBackendBag(20.0, 10.5);
        FlightBackendBag flightTwoBckBag = new FlightBackendBag(30.0, 20.5);

        FlightBackendData flightOneBckDataDest1 = new FlightBackendData("Oporto", 20.0, flightOneBckBag);
        FlightBackendData flightTwoBckDataDest1 = new FlightBackendData("Oporto", 30.0, flightTwoBckBag);

        FlightBackendData flightOneBckDataDest2 = new FlightBackendData("Lisbon", 10.0, flightOneBckBag);
        FlightBackendData flightTwoBckDataDest2 = new FlightBackendData("Lisbon", 20.0, flightTwoBckBag);

        List<FlightBackendData> flightBackendDataListDest1 = Arrays.asList(flightOneBckDataDest1, flightTwoBckDataDest1);
        List<FlightBackendData> flightBackendDataListDest2 = Arrays.asList(flightOneBckDataDest2, flightTwoBckDataDest2);

        FlightBackend flightBackendDest1 = new FlightBackend(flightBackendDataListDest1);
        FlightBackend flightBackendDest2 = new FlightBackend(flightBackendDataListDest2);

        Map<String, FlightDestAvg> flightDtoResponseCurrent = new HashMap<>();
        flightDtoResponseCurrent.put(flyFrom,new FlightDestAvg("Porto","EUR",2,(20.0+30.0)/2,new FlightBagAvg((20.0+30.0)/2,(10.5+20.5)/2)));
        flightDtoResponseCurrent.put(flyTo,new FlightDestAvg("Lisbon Portela","EUR",2,(10.0+20.0)/2,new FlightBagAvg((20.0+30.0)/2,(10.5+20.5)/2)));

        // act
        Mockito
                .when(locationService.getAirportName(Mockito.anyString()))
                .thenReturn("Porto","Lisbon Portela");

        Mockito
                .when(restTemplate.getForEntity(Mockito.anyString(), Matchers.any(Class.class)))
                .thenReturn(new ResponseEntity(flightBackendDest1, HttpStatus.OK), new ResponseEntity(flightBackendDest2, HttpStatus.OK));


        Map<String, FlightDestAvg> flightDtoResponseActual = flightService.getFlightsInformation(flyFrom, flyTo, curr, dateFrom, dateTo,null);
        // assert
        Assert.assertEquals(flightDtoResponseCurrent.get("OPO"), flightDtoResponseActual.get("OPO"));
        Assert.assertEquals(flightDtoResponseCurrent.get("LIS"), flightDtoResponseActual.get("LIS"));
    }

    @Test
    public void getFlightsInformation_withNullBags_ReturnsListOfFlightsManipulated() {
        // arrange
        String flyFrom = "OPO";
        String flyTo = "LIS";
        String curr = "EUR";
        String dateFrom = "20/02/2021";
        String dateTo = "20/04/2021";

        UriComponentsBuilder uriBuilderFlyFromToFlyToOne = UriComponentsBuilder.fromUriString("https://api.skypicker.com/flights")
                .queryParam("partner", "picky")
                .queryParam("flyFrom", flyFrom)
                .queryParam("fly_to", flyTo)
                .queryParam("curr", curr)
                .queryParam("dateFrom", dateFrom)
                .queryParam("dateTo", dateTo);

        FlightBackendBag flightOneBckBag = new FlightBackendBag(20.0, 10.5);
        FlightBackendBag flightTwoBckBag = new FlightBackendBag();

        FlightBackendData flightOneBckDataDest1 = new FlightBackendData("Oporto", 20.0, flightOneBckBag);
        FlightBackendData flightTwoBckDataDest1 = new FlightBackendData("Oporto", 30.0, flightTwoBckBag);

        FlightBackendData flightOneBckDataDest2 = new FlightBackendData("Lisbon", 10.0, flightOneBckBag);
        FlightBackendData flightTwoBckDataDest2 = new FlightBackendData("Lisbon", 20.0, flightTwoBckBag);

        List<FlightBackendData> flightBackendDataListDest1 = Arrays.asList(flightOneBckDataDest1, flightTwoBckDataDest1);
        List<FlightBackendData> flightBackendDataListDest2 = Arrays.asList(flightOneBckDataDest2, flightTwoBckDataDest2);

        FlightBackend flightBackendDest1 = new FlightBackend(flightBackendDataListDest1);
        FlightBackend flightBackendDest2 = new FlightBackend(flightBackendDataListDest2);

        Map<String, FlightDestAvg> flightDtoResponseCurrent = new HashMap<>();
        flightDtoResponseCurrent.put(flyFrom,new FlightDestAvg("Porto","EUR",2,(20.0+30.0)/2,new FlightBagAvg(20.0/2,10.5/2)));
        flightDtoResponseCurrent.put(flyTo,new FlightDestAvg("Lisbon Portela","EUR",2,(10.0+20.0)/2,new FlightBagAvg(20.0/2,10.5/2)));

        // act
        Mockito
                .when(locationService.getAirportName(Mockito.anyString()))
                .thenReturn("Porto","Lisbon Portela");

        Mockito
                .when(restTemplate.getForEntity(Mockito.anyString(), Matchers.any(Class.class)))
                .thenReturn(new ResponseEntity(flightBackendDest1, HttpStatus.OK), new ResponseEntity(flightBackendDest2, HttpStatus.OK));

        Map<String, FlightDestAvg> flightDtoResponseActual = flightService.getFlightsInformation(flyFrom, flyTo, curr, dateFrom, dateTo,null);
        // assert
        Assert.assertEquals(flightDtoResponseCurrent.get("OPO"), flightDtoResponseActual.get("OPO"));
        Assert.assertEquals(flightDtoResponseCurrent.get("LIS"), flightDtoResponseActual.get("LIS"));
    }

}

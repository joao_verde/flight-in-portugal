package com.flightinportugal.flightinformation.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(MockitoJUnitRunner.class)
public class LocationServiceTest {

    @InjectMocks
    private final LocationService locationService = new LocationService();

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void getAirportName_withSuccess_ReturnsAirportName() {
        // arrange
        String locations = "{\"locations\":[{\"id\":\"LIS\",\"int_id\":856,\"airport_int_id\":856,\"active\":true,\"code\":\"LIS\",\"icao\":\"LPPT\",\"name\":\"Lisbon Portela\",\"slug\":\"lisbon-portela-lisbon-portugal\",\"slug_en\":\"lisbon-portela-lisbon-portugal\",\"alternative_names\":[\"Aeroporto de Lisboa\"],\"rank\":25}]}";
        String expectedAirportName = "Lisbon Portela";
        String baseUrlLocation = "https://api.skypicker.com" + "/locations";
        String iataCode = "LIS";
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(baseUrlLocation)
                .queryParam("term", iataCode)
                .queryParam("location_types", "airport")
                .queryParam("limit", 1);
        // act
        Mockito
                .when(restTemplate.getForEntity(uriBuilder.toUriString(), String.class))
                .thenReturn(new ResponseEntity(locations, HttpStatus.OK));

        String currentAirportName = locationService.getAirportName(iataCode);
        // assert
        Assert.assertEquals(expectedAirportName, currentAirportName);

    }
}

# flight-in-portugal
Provides flights information between Oporto ->Lisbon or Lisbon -> Oporto from the company TAP and Ryanair

This guide shows how easy to run the project for get flight information between OPO and LIS.

Software require to run this project: 
- Java 11
- Docker Desktop
- Maven
- API Testing(Postman) or CURL


Containers installations:

- Redis

Run the following command in the redis folder: 

$ docker-compose up -d

- Spring Boot App

Run the following command in the spring folder

$ docker build -t flight .

$ docker run -d -p 8080:8080 -t flight

Important to consider: 

If you run the application and visit http://localhost:6379/flight/avg/.... you would get the error .ConnectTimeoutException: connection timed out.
This is simply because we want our two docker images to talk to each other and by default they sit on their own network and are isolated from each other.
Basically is missing a docker-compose to group both container or create a private network to allow private communication.

I would kindly to ask you to test the application running directly in intellij for instance, but make sure the redis container is running in your local desktop

-----------------------------------------------------------------------------------------------------------------------------------
**Test the Endpoints**

Get flights information between two specific destinations (for instance OPO and LIS)

METHOD: GET 

URI w/all parameters: http://localhost:8080/flight/avg?dest1=OPO&dest2=LIS&curr=EU&dateFrom=25/01/2021&dateTo=27/01/2021&select_airlines=FR,TP

Query parameters specification:
- dest1= departure location
- dest2= arrival destination
- curr= price of the itinerary in currency specified
- dateFrom= search flights from this date (dd/mm/YYYY)  			 [OPTIONAL]
- dateTo= search flights upto this date (dd/mm/YYYY)	  			 [OPTIONAL]
- select_airlines= list of airlines (IATA codes) separated by ‘,’	 [OPTIONAL]

dateFrom, dateTo and select_airlines are optional parameters. If each value is null, it is not being sent to backend server api.

If dateFrom and dateTo are null, the backend will retrieve all flights available with no specific period.

If select_airlines is null, the backend will retrieve all airlines flights 


------------------------------------------------------------------
List all the records of the requests in database (if exists);

METHOD: GET 

URI: http://localhost:8080/api-calls

The application only logs the requests from /flight/avg controller.

------------------------------------------------------------------
Delete all the records of the requests from database (if exists)

METHOD: DELETE 

URI: http://localhost:8080/api-calls

-----------------------------------------------------------------------------------------------------------------------------------
**Database - NoSQL Mongo DB**

For the database, I'm using the MongoDB Atlas which is a fully-managed cloud database service. Instead of using a container, I preferred to use a service cloud allowing the access from anywhere. 

Network Acess:
- Allow Access from Anywhere

Database User Acess:

- Username - cocus
- Password - pass1234
- MongoDB Roles - admin

Connect to the cluster using for instance MongoDB Compass UI

- mongodb+srv://cocus:<password>@cluster0.pz5ai.mongodb.net/test
- mongodb+srv://cocus:pass1234@cluster0.pz5ai.mongodb.net/test


-----------------------------------------------------------------------------------------------------------------------------------
**Cache - Spring Boot in-memory cache**

This project uses a cache mechanism to enhance the performance of the system. Cache is used for flights price average information, where it is defined one hour for the ttl. The flight price changes over time across the day and I consider one hour enough to get information from the in-memory cache. After this time, the data is removed from cache, which means the next request will be forward to backend server to get the up to date flights average price.

Some properties defined: 

- spring.cache.type=redis
- spring.redis.database=0
- spring.redis.host=localhost
- spring.redis.port=6379
- spring.redis.password=redispassword
- spring.cache.redis.time-to-live=3600000



-----------------------------------------------------------------------------------------------------------------------------------
**Unit Tests**

There are only tests for services class which have the business logic of the solution.

FlightService and LocationService

Mockito used for mock object dependency, returning a customize response to test each piece of code.

Frameworks:
- JUnit 5
- Mockito


-----------------------------------------------------------------------------------------------------------------------------------
**Maven Dependency Manager**

Below are descripted few dependencies used in the project: 
- spring-boot-starter-data-mongodb: It is used for MongoDB document-oriented database and Spring Data MongoDB.
- spring-boot-starter-web: It is used for building the web application, including RESTful applications using Spring MVC (Spring MVC to handle web requests)
- spring-boot-starter-actuator: It is used for Spring Boot's Actuator that provides production-ready features to help you monitor and manage your application.
- Lombok: reduce boilerplate code for model/data objects
- commons-math3: library of lightweight, self-contained mathematics and statistics components addressing the most common problems not available in the Java programming language or Commons Lang
- spring-boot-starter-data-redis: provides the abstractions of the Spring Data platform to Redis


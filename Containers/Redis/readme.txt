NOTES:
1) setting the requirepass configuration directive causes the server to require password authentication with the AUTH command before sending other commands.
2) redis-cli is the Redis command line interface, a simple program that allows to send commands to Redis


- Run the following command in the redis folder
$ docker-compose up -d

- run arbitrary commands inside an existing container
docker exec -it <mycontainer> bash


- access to the redis-cli
#more completed
redis-cli -h host.domain.com -p port -a yourpassword
#only with password
redis-cli -a redispassword